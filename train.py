# Adjusted from https://github.com/kserve/kserve/tree/release-0.9/docs/samples/v1beta1/lightgbm

import lightgbm as lgb
from sklearn.datasets import load_iris
import os
import shutil

model_dir = "model"
BST_FILE = "model.bst"

print("Cleaning up model directory..")
shutil.rmtree(model_dir)
os.mkdir(model_dir)
print("..model directory cleaned")

print("Loading iris dataset..")
iris = load_iris()
y = iris['target']
X = iris['data']
dtrain = lgb.Dataset(X, label=y)
print("..Iris dataset loaded")

params = {
    'objective':'multiclass',
    'metric':'softmax',
    'num_class': 3
}

print("Start training LightGBM model..")
lgb_model = lgb.train(params=params, train_set=dtrain)
print("... finished training LightGBM model")

model_file = os.path.join(model_dir, BST_FILE)
lgb_model.save_model(model_file)
print(f"Saved LightGBM bosterfile {model_file}")
