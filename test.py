import deeploy
from deeploy import Client
import json
import numpy as np
print(f'deeploy verion is: {deeploy.__version__}')


access_key = input('Add your personal access key:  ')
secret_key = input('Add your personal secret key:  ')
deeploy_host = input('Add the URL where you run Deeploy (e.g. app.deeploy.ml)')
workspace_id = input('Add the Deeploy workspace ID where you want to deploy your model. You can find this in the UI workspace settings:  ')
print('NOTE: the input.json file in the root of the repository is used to make a request to the model:  ')
deployment_id = input('Add the Deeploy deployment ID of your freshly deployed model. You can find this in the UI deployment details/api:  ')

with open('input.json', encoding='utf-8') as f:
    request_body = json.load(f)

with open('metadata.json', encoding='utf-8') as f:
    metadata = json.load(f)

client_options = {
    'access_key': access_key,
    'secret_key': secret_key,
    'host': deeploy_host,
    'workspace_id': workspace_id,
}
client = Client(**client_options)

prediction = client.predict(deployment_id, request_body)
predicted_class = [predictionClass for predictionClass, index in metadata['predictionClasses'].items() if index == np.argmax(prediction.predictions)]

print(f"Predicted class is: {predicted_class[0]}")
