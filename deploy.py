import deeploy
from deeploy import Client
from deeploy.enums import ModelType
print(f'deeploy verion is: {deeploy.__version__}')

access_key = input('Add your personal acces key  ')
secret_key = input('Add your personal secret key  ')
deeploy_host = input('Add the URL where you run Deeploy  ')
print('NOTE: make sure you aleady added this repository to your Deeploy workspace and committed your latest changes')
workspace_id = input('Add the Deeploy workspace ID where you want to deploy your model  ')


client_options = {
    'access_key': access_key,
    'secret_key': secret_key,
    'host': deeploy_host,
    'workspace_id': workspace_id,
}
client = Client(**client_options)

deploy_options = deeploy.DeployOptions(**{
        'name': 'My LightGBM Deployment Name',
        'description': 'My LightGBM Deployment Description',
    })

client.deploy(
    options=deploy_options,
    overwrite_contract=False,
    overwrite_metadata=False,
    model_type=ModelType.LIGHTGBM.value,
    local_repository_path='.'
    )
