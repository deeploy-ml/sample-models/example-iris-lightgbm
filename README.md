# E2E Iris example using LightGBM
This example repo shows how to train, deploy and test a LightGBM model for use in [Deeploy](https://deeploy.ml) using the [Deeploy Python Client](https://pypi.org/project/deeploy/). The train, deploy and test steps are seperated in .py files and can act as a starting point for your own implementation (for more information, see [train](#train), [deploy](#deploy), [test](#test) below). Moreover you will find three .json files in the repo:
* The `metadata.json` file shares metadata about your model with Deeploy during the creation of a deployment
* The `model/reference.json` file shares where to download the model artefact when the artefact itself is not present in the model folder
* The `input.json` shows an example request body to inference the model

For more infromation on how to prepare a repository for integration with Deeploy check [this article](https://deeploy-ml.zendesk.com/hc/en-150/articles/4411887195666-Preparing-a-repository) in our documentation.

## Dataset
[The Iris flower data set](https://archive.ics.uci.edu/ml/datasets/iris) or Fisher's Iris data set is a multivariate data set introduced by the British statistician, eugenicist, and biologist Ronald
Fisher in his 1936 paper *The use of multiple measurements in taxonomic problems* as an example of linear discriminant analysis.

The data set consists of 50 samples from each of three species of Iris (Iris setosa, Iris virginica and Iris versicolor). Four features were measured
from each sample: the length and the width of the sepals and petals, in centimeters. Based on the combination of these four features, Fisher developed
a linear discriminant model to distinguish the species from each other.

## Prerequisites

Make sure you have a python environment with the required libraries installed:
```bash
pip install -r requirements.txt
```

## Train
A very simple LightGBM model is trained and exported as a [saved Booster model](https://lightgbm.readthedocs.io/en/latest/pythonapi/lightgbm.Booster.html#lightgbm.Booster.save_model) in the [`train.py`](./train.py). To train the model locally execute line of code in your terminal from the root of the directory:

```bash
python train.py
```

As a result the reference to the pre-trained LightGBM model in our example s3 bucket in the `/model` folder is overwritten by a `model.bst` file. This model complies with the [supported model version in Deeploy](https://deeploy-ml.zendesk.com/hc/en-150/articles/4411974086162-Supported-Framework-Versions-for-KServe).

> Make sure to commit your new changes if you want to include them in your deployment before continuing

## Deploy
To interactively deploy from your repo to your Deeploy installation, excute the command below from the root of this directory in your terminal.
This code example uses the Deeploy Python Client.

```bash
python deploy.py
```

## Test
To interactively test a created deployment by inferencing it, excute the command below from the root of this directory in your terminal.
This code example uses the Deeploy Python Client.
```bash
python test.py
```
